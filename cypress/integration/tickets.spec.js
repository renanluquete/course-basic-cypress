describe("Tickets", () => {

   beforeEach(() => {
      cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html")
   })

   it("fills all the text input fields", () => {
      cy.get("#first-name").type("Charlie")
      cy.get("#last-name").type("Brown Jr")
      cy.get("#email").type("cbrjr@gmail.com")
      cy.get("#requests").type("skateboard na veia")
      cy.get("#signature").type("cbrjr")
   })

   it("select two tickets", () => {
      cy.get("#ticket-quantity").select("2")
   })

   it("select VIP ticket type", () => {
      cy.get("#vip").check()
   })

   it("selects 'social media' checkbox", () => {
      cy.get("#social-media").check()
   })

   it("selects 'friend' and 'publication' and uncheck 'friend' checkbox", () => {
      cy.get("#friend").check()
      cy.get("#publication").check()
      cy.get("#friend").uncheck()
   })

   it("given load with success, when open link, then contains text 'TICKETBOX' in the header", () => {
      // Assert
      cy.get("header h1").should("have.text", "TICKETBOX")
   })

   it("given insert invalid email, when fill input email, then contains invalid class", () => {
      // Arrange, Act
      cy.get("#email").type("cbrjr-gmail.com")
      // Assert
      cy.get("#email.invalid").should("exist")

      // Arrange, Act
      cy.get("#email")
          .clear()
          .type("cbrjr@gmail.com")
      // Assert
      cy.get("#email.invalid").should("not.exist")
   })

   it("given fill name, last name, ticket quantity and ticket type, when fill form, then make correct phrase Purchase Agreement", () => {
      cy.get("#first-name").type("Charlie")
      cy.get("#last-name").type("Brown Jr")
      cy.get("#ticket-quantity").select("2")
      cy.get("#vip").check()

      cy.get(".agreement p")
          .should(
              "have.text",
              "I, Charlie Brown Jr, wish to buy 2 VIP tickets. I understand that all ticket sales are final."
          )
   })

   it("given fill all mandatory inputs, when fill form, should enable button", () => {
      // Arrange, Act
      cy.get("#first-name").type("Charlie")
      cy.get("#last-name").type("Brown Jr")
      cy.get("#email").type("cbrjr@gmail.com")
      cy.get("#ticket-quantity").select("2")
      cy.get("#vip").check()
      cy.get("#agree").check()
      // Assert
      cy.get("button[type='submit']").should("not.be.disabled")
   })

   it("given click in reset, when click in reset, should clear all fields", () => {
      // Arrange
      cy.get("#first-name").type("Charlie")
      cy.get("#last-name").type("Brown Jr")
      cy.get("#email").type("cbrjr@gmail.com")
      cy.get("#requests").type("skateboard na veia")
      cy.get("#signature").type("cbrjr")
      cy.get("#ticket-quantity").select("2")
      cy.get("#vip").check()
      cy.get("#friend").check()
      cy.get("#agree").check()
      // Act
      cy.get("button[type='submit']").click()
      // Assert
      cy.get("#first-name").should("be.empty")
      cy.get("#last-name").should("be.empty")
      cy.get("#email").should("be.empty")
      cy.get("#requests").should("be.empty")
      cy.get("#signature").should("be.empty")
      cy.get("#ticket-quantity").should("have.value", "1")
      cy.get("#vip").should("be.empty")
      cy.get("#friend").should("be.empty")
      cy.get("#agree").should("be.empty")
      cy.get(".agreement p")
          .should(
              "have.text",
              "I, , wish to buy 1 General Admission ticket. I understand that all ticket sales are final."
          )
   })

   it("fills mandatory fields, using support command", () => {
      // Arrange, Act
      const costumer = {
         name: "Charlie",
         lastName: "Brown Jr",
         email: "cbrjr@gmail.com"
      }
      cy.fillMandatoryFields(costumer)
      // Assert
      cy.get("button[type='submit']").should("not.be.disabled")
   })
})